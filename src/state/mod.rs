use std::process::Command;
use Department;
use Employee;

#[derive(Debug)]
pub struct State {
    departments: Vec<Department>,
    employees: Vec<Employee>,
}

impl State {
    pub fn new() -> Self {
        State {
            departments: vec![],
            employees: vec![],
        }
    }

    pub fn insert_department(&mut self, department: Department) {
        self.departments.push(department);
    }

    pub fn insert_employee(&mut self, employee: Employee) {
        self.employees.push(employee);
    }

    pub fn list_departments(&self) {
        Command::new("clear").status().unwrap();
        for department in self.departments.iter() {
            println!("{}", department.name);
        }
    }

    pub fn list_employees(&self) {
        Command::new("clear").status().unwrap();
        for employee in self.employees.iter() {
            println!("ID: {}\nName: {}", employee.id, employee.name);
        }
    }
}

