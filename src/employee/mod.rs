#[derive(Debug)]
pub struct Employee {
    pub id: u32,
    pub name: String,
}

impl Employee {
    pub fn new(name: String) -> Employee {
        unsafe {
            ::EMPLOYEES_ID += 1;
            return Employee {
                id: ::EMPLOYEES_ID,
                name: name,
            };
        }
    }
}

