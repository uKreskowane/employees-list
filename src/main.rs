static mut EMPLOYEES_ID: u32 = 0;

mod department;
mod employee;
mod menu;
mod state;

use std::io;
use std::process::Command;

use department::Department;
use employee::Employee;
use menu::Menu;
use state::State;

fn main() {
    let mut state: State = State::new();
    Command::new("clear").status().unwrap();
    loop {
        let input: String;
        Menu::init();
        input = Menu::read_input();
        recognize_option(&input, &mut state);
    }
}

fn recognize_option(input: &str, state: &mut State) {
    if input == "D" {
        let mut name = String::new();
        match io::stdin().read_line(&mut name) {
            Ok(_) => {
                name = String::from(name.trim());
                state.insert_department(Department::new(name));
            },
            Err(_) => println!("Something went wrong.")
        }
        Command::new("clear").status().unwrap();
    }
    else if input == "E" {
        let mut name = String::new();
        match io::stdin().read_line(&mut name) {
            Ok(_) => {
                name = String::from(name.trim());
                state.insert_employee(Employee::new(name));
            },
            Err(_) => println!("Something went wrong.")
        }
        Command::new("clear").status().unwrap();
    }
    else if input == "LD" {
        state.list_departments();
        wait_and_clear();
    }
    else if input == "LE" {
        state.list_employees();
        wait_and_clear();
    }
    else if input == "Q" {
        println!("Quit.");
    }
    else {
        println!("Unknown option {}. Try again.", input);
    }
}

fn wait_and_clear() {
    let mut exit = String::new();
    println!("Press enter to continue...");
    io::stdin().read_line(&mut exit).unwrap();
    Command::new("clear").status().unwrap();
}

