use std::io;

pub struct Menu {}

impl Menu {
    pub fn init() {
        println!("Welcome in \"Employees list\".");
        println!("You can create department and join employees to them");
        println!("What do you want to do?");
        Menu::write_options();
    }

    pub fn read_input() -> String {
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_) => input = String::from(input.trim()),
            Err(e) => {
                println!("Something went wrong. Cannot read input.");
                println!("Details:\n{}", e);
            }
        };
        input
    }

    fn write_options() {
        println!("[D] Create new department.");
        println!("[E] Create new employee.");
        println!("[LD] List all departments.");
        println!("[LE] List all employees.");
        println!("[Q] Quit.");
    }
}

