use employee::Employee;

#[derive(Debug)]
pub struct Department {
    pub name: String,
    employees: Vec<Employee>,
}

impl Department {
    pub fn new(name: String) -> Department {
        Department {
            name: name,
            employees: Vec::new()
        }
    }

    pub fn add_employee(&mut self, employee: Employee) {
        self.employees.push(employee);
    }
}

